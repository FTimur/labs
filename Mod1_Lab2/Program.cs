﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod1_Lab2
{
    class Programs
    {
        static void Main(string[] args)
        {

            //Создание нового объекта Car1 класс Car
            var Car1 = new Car();
            //Описание свойств объекта
            Car1.Color = "White";
            Car1.Year = 2011;
            Car1.Mileage = 11000;
            //Создание нового объекта 
            var Car2 = new Car("Red", 2008);
            //Сообщение в консоли
            int carCount = Car.CountCars();
            Console.WriteLine($"Сейчас в наличии есть {carCount} машин(ы).");
        }

    }
    // Описание класса
    public class Car
    {
        public string Color { get; set; }
        public int Year { get; set; }
        public int Mileage { get; set; }


        //Создание конструктора
        //Конструктор создает объект когда есть цвет и год выпуска
        public Car(string color, int year)
        {
            this.Color = color;
            this.Year = year;
            instances++;
        }
        //Конструктор создает объект при наличии года выпуска и пробега
        public Car(int year, int mileage)
        {
            this.Year = year;
            this.Mileage = mileage;
            instances++;
        }
        //Создание целочисленной переменной "Instens", присваение ей значения "0"
        private static int instances = 0;
        // При запуске конструктора увеличение экземпляров
        public Car()
        {
            instances++;
        }
        // При запуске констурктора подсчитывается сумма объектов
        public static int CountCars()
        {
            return instances;
        }
    }
}    


