﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod1_Lab4
{
    public class Degree
    {
        public string Name { get; set; }
        public Course Course { get; set; }

        public Degree(string name, Course Course)
        {
            this.Name = name;
            this.Course = Course;
        }
    }
}