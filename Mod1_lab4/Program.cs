﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod1_Lab4
{
    class Programs
    {
        static void Main(string[] args)
        {
            var Teacher1 = new Teacher("Philip", 45, "mail");
            var Student1 = new Student("Billy", 12);
            var Student2 = new Student("Dilly", 12);
            var Student3 = new Student("Willy", 12);
            var Student4 = new Student("Killy", 12);
            List<Student> group = new List<Student> { Student1, Student2, Student3, Student4 };
            var Course1 = new Course("Programming with C#", Teacher1, group);
            var Degree1 = new Degree("bachelor", Course1);
            var Educational_Programs1 = new Educational_programs("Information Technology", Degree1);
            
            


            //Сообщение в консоли
            int StudentCount = Student.CountStudent();
            Console.WriteLine($"Программа {Educational_Programs1.Name}, степень {Degree1.Name}.");
            Console.WriteLine($"Курс {Course1.Name},степень {Degree1.Name}.");
            Console.WriteLine($"На курсе {Course1.Name} учится {StudentCount} студента.");

        }

    }
}


