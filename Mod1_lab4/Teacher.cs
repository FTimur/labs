﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod1_Lab4
{
    public class Teacher
    {
        public string Teacher_name { get; set; }
        public int Age { get; set; }
        public string Gender { get; set; }

        public Teacher(string Teacher_name, int Age, string Gender)
        {
            this.Teacher_name = Teacher_name;
            this.Age = Age;
            this.Gender = Gender;
        }

    }

}