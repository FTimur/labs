﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod1_Lab4
{
    public class Course 
    {
        
        public string Name { get; set; }
        public Teacher Teacher { get; set; }
        List<Student> group { get; set; }
               

        public Course(string name, Teacher teacher, List<Student> group)
        {
            this.Name = name;
            this.Teacher = teacher;
            this.group = group;
        }
    }

}