﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod1_Lab4
{
    // Описание класса
    public class Student
    {
        private static int instances = 0;
        
        public string Name { get; set; }
        public int Age { get; set; }
            public Student(string name, int age)
        {
            this.Name = name;
            this.Age = age;
            instances++;
        }
        public static int CountStudent()
        {
            return instances;
        }
    }
}
