﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod1_Lab3
{
    class Programs
    {
        static void Main(string[] args)
        {

            //Создание нового объекта Car1 класс Car
            var Car1 = new Car();
            //Описание свойств объекта
            Car1.Color = "White";
            Car1.Year = 2011;
            Car1.Mileage = 11000;
            //Создание нового объекта 
            var Car2 = new Car("Red", 2008);
            //Cоздание нового объекта 
            var Car3 = new Car(25000, 2005);
            // Почему если у объекта нет свойств, нет ошибки?
            var Car4 = new Car();
            //Сообщение в консоли
            int carCount = Car.CountCars();
            Console.WriteLine($"Сейчас в наличии есть {carCount} машин(ы).");
        }

    }
 }


