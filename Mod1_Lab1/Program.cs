﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod1_Lab1
{
    class Programs
    {
        static void Main(string[] args)
        {
            //Создание нового объекта Car1 класс Car
            var Car1 = new Car();
            //Описание свойств объекта
            Car1.Color = "White";
            Car1.Year = 2010;
            Car1.Mileage = 11000;
            //Сообщение в консоли
            Console.WriteLine($"This car is painted {Car1.Color}, was built in {Car1.Year}, and has {Car1.Mileage} miles on it");
        }
    }
    // Создание нового класса Car
    public class Car
    {
        // Описание класса
        public string Color { get; set; }
        public int Year { get; set; }
        public int Mileage { get; set; }

    }
}

