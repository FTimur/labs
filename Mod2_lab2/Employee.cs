﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod2_Lab2
{
    abstract class Employee
    {
       
        private static int employeeCount = 1;
                
        private string employeeName;
        private double employeeBaseSalary;
        private int employeeId;

        public string Name
        {
            get
            {
                return employeeName;
            }

            set
            {
                employeeName = value;
            }
        }
        public double BaseSalary
        {
            get
            {
                return employeeBaseSalary;
            }

            set
            {
                employeeBaseSalary = value;
            }
        }
        public int ID
        {
            get
            {
                return employeeId;
            }

            set
            {
                employeeId = value;
            }
        }

        // Конструктор
        public Employee(String name, double baseSalary)
        {
            this.Name = name;
            this.BaseSalary = baseSalary;
            this.ID = employeeCount++;
        }

        // Этот метод возвращает basesalary
        public double getBaseSalary()
        {
            return this.BaseSalary;
        }

        // Этот метод возвращает имя сотрудника - name
        public String getName()
        {
            return this.Name;
        }

        // Этот метод взвращает ID
        public int getEmployeeID()
        {
            return this.ID;
        }

        // Этот метод возвращает ID и Name
        public String toString()
        {
            return this.ID + " " + this.Name;
        }

        // 
        public abstract String employeeStatus();
        
    }
}

