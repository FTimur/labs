﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod2_Lab3
{
    // Описание класса
    public class Student : Person
    {
        private static int instances = 0;
        private static double studentsID = 1001;

        private double studentId;

        // Public properties
        public double Id
        {
            get
            {
                return studentId;
            }

            set
            {
                studentId = value;
            }
        }


        public Student(string name, int age) : base (name, age)
        {
            this.Id = studentsID++;
            instances++;
        }
              
                
        // При запуске констурктора подсчитывается сумма объектов
        public static int CountStudent()
        {
            return instances;
        }
        
        public string TakeTest(string CourseName)
        {
            return this.Id + this.Name + " учится на курсе " + CourseName;
        }

    }
}
