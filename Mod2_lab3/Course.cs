﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod2_Lab3
{
    public class Course
    {
               

        public string Name { get; set; }
        public Teacher Teacher { get; set; }         
        
        public List<Student> group { get; set; }


        public Course(string name, Teacher Teacher, List<Student> group)
        {
            this.Name = name;
            this.Teacher = Teacher;
            this.group = group;
        }
        
    }

}