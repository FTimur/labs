﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod2_Lab3
{
    public class Teacher : Person
    {
        private static double teacherId = 101;

        private double teachertId;

        // Public properties
        public double Id
        {
            get
            {
                return teacherId;
            }

            set
            {
                teacherId = value;
            }
        }


        public Teacher(string name, int age) : base(name, age)
        {
            this.Id = teacherId++;
            
        }
        public string GradeTest(string CourseName)
        {
            return this.Id + this.Name + " преподает на курсе " + CourseName;
        }

    }

}