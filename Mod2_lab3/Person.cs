﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod2_Lab3
{
    // Описание класса
    public abstract class Person
    {
        private string personName;
        private int personAge;
        

        public string Name
        {
            get 
            { 
                return personName; 
            }
            set
            {
                personName = value;
            }
        }

        public int Age
        {
            get 
            { 
                return personAge; 
            }
            set 
            { 
                personAge = value; 
            }
        }
        
        public Person(string name, int age)
        {
            this.Name = name;
            this.Age = age;
        }
        public string getName()
        {
            return this.Name;
        }
    }
}