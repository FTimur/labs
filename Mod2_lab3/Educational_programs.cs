﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod2_Lab3
{
    public class Educational_programs
    {
        public string Name { get; set; }
        public Degree Degree { get; set; }

        public Educational_programs(string Name, Degree Degree)
        {
            this.Name = Name;
            this.Degree = Degree;
        }
    }

}